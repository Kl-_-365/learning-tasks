# Создать пять классов описывающие реальные объекты. Каждый класс
# должен содержать минимум три приватных атрибута, конструктор, геттеры
# и сеттеры для каждого атрибута, два метода

# Create class Animal
class Animal(object):
    __count = 0

    @staticmethod
    def population():
        return f'Population of animals is {Animal.__count}'

    def __init__(self, name, age, weight, height):
        self.__name = name
        self.__age = age
        self.__weight = weight
        self.__height = height
        Animal.__count += 1

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, new_name):
        if new_name == "":
            print("The name cannot be empty ")
        else:
            self.__name = new_name

    @property
    def age(self):
        return self.__age

    @age.setter
    def age(self, new_age):
        if not new_age.isdigit():
            print("The age must be a digit")
        else:
            self.__age = new_age

    @property
    def weight(self):
        return self.__weight

    @weight.setter
    def weight(self, new_weight):
        if not new_weight.isdigit():
            print("The weight must be a digit")
        else:
            self.__weight = new_weight

    @property
    def height(self):
        return self.__height

    @height.setter
    def height(self, new_height):
        if not new_height.isdigit():
            print("The height must be a digit")
        else:
            self.__height = new_height

    def __str__(self):
        return f'My name is {self.__name}, my age is {self.__age}, my weight is  {self.__weight}, my height is {self.__height}'

    def __del__(self):
        Animal.__count -= 1

    def born(self, name, weight, height):
        return Animal(name, 0, weight, height)

    def greeting(self):
        return f"Hello, friends! I'm {self.__name}!"


# Create class Cat
class Cat(Animal):
    __count_of_cats = 0

    @staticmethod
    def population_of_cats():
        return f'Population of cats is {Cat.__count_of_cats}'

    def __init__(self, name, age, weight, height, breed):
        super().__init__(name, age, weight, height)
        self.__breed = breed
        Cat.__count_of_cats += 1

    def __del__(self):
        super().__del__()
        Cat.__count_of_cats -= 1

    def __str__(self):
        return super().__str__() + f" my breed is {self.__breed}"

    @property
    def breed(self):
        return self.__breed

    @breed.setter
    def breed(self, new_breed):
        if new_breed == "":
            print("The breed cannot be empty ")
        else:
            self.__breed = new_breed

    @classmethod
    def pet_a_cat(cls):
        return "Mur-mur-mur"

    @classmethod
    def kick_a_cat(cls):
        return "Frrrrrrrr...."


class Home_cat(Cat):
    def open_the_fridge(self):
        return self.scold_cat()

    def scold_cat(self):
        return "You fat bastard!!!"


class Street_cat(Cat):
    def feed(self):
        return "Thank you, I'm full"

    def lack_of_equity(self):
        return "...looks sadly at the house cat..."
