# Создать класс Car. Атрибуты: марка, модель, год выпуска, скорость(по
# умолчанию 0). Методы: увеличить скорости(скорость + 5), уменьшение
# скорости(скорость - 5), стоп(сброс скорости на 0), отображение скорости,
# разворот(изменение знака скорости). Все атрибуты приватные

class Car(object):
    def __init__(self, brand, model, year, speed=0):
        self.__brand = brand
        self.__model = model
        self.__year = year
        self.__speed = speed

    def __str__(self):
        return f"Brand: {self.__brand}, Model: {self.__model}, Year: {self.__year}, Speed: {self.__speed}"

    @property
    def brand(self):
        return self.__brand

    @property
    def model(self):
        return self.__model

    @property
    def year(self):
        return self.__year

    @property
    def speed(self):
        return self.__speed

    @speed.setter
    def speed(self, new_speed):
        try:
            float(new_speed)
            self.__speed = new_speed
        except ValueError:
            print("Entered false value")

    def increase_the_speed(self):
        self.__speed += 5

    def decrease_the_speed(self):
        self.__speed -= 5

    def stop(self):
        self.__speed = 0

    def speed_display(self):
        print(f"Auto's speed is {self.__speed}")

    def reverse_speed(self):
        self.__speed *= -1

car = Car('LADA', 'VESTA', 2018, 12)
print(car)