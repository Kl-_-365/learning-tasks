# Task 7.2

# Написать программу, со следующим интерфейсом: пользователю предоставляется на
# выбор 12 вариантов перевода(описанных в первой задаче). Пользователь вводит цифру
# от одного до двенадцати. После программа запрашивает ввести численное значение.
# Затем программа выдает конвертированный результат. Использовать функции из первого
# задания. Программа должна быть в бесконечном цикле. Код выхода из программы - “0”

from homework import inch_to_cm, cm_to_inch, ml_to_km, km_to_ml, ft_to_kg, kg_to_ft, oz_to_g, g_to_oz, gallon_to_l, \
    l_to_gallon, pint_to_l, l_to_pint


def user_input():
    num_operation = int(input("Enter number from 0 to 12: \n"))
    value = int(input("Enter value for convert: \n"))
    return num_operation, value


def converter():
    while True:
        user_values = user_input()
        num_operation = user_values[0]
        value = user_values[1]
        if num_operation == 0:
            break
        elif num_operation == 1:
            print(inch_to_cm(value))
        elif num_operation == 2:
            print(cm_to_inch(value))
        elif num_operation == 3:
            print(ml_to_km(value))
        elif num_operation == 4:
            print(km_to_ml(value))
        elif num_operation == 5:
            print(ft_to_kg(value))
        elif num_operation == 6:
            print(kg_to_ft(value))
        elif num_operation == 7:
            print(oz_to_g(value))
        elif num_operation == 8:
            print(g_to_oz(value))
        elif num_operation == 9:
            print(gallon_to_l(value))
        elif num_operation == 10:
            print(l_to_gallon(value))
        elif num_operation == 11:
            print(pint_to_l(value))
        elif num_operation == 12:
            print(l_to_pint(value))
        else:
            print("Wrong number entered")
    return


converter()
