from tkinter import *
from tkinter.ttk import Combobox
from HW_13.func_HW_13 import Calculator


def clicked():
    number1 = txt1.get()
    number2 = txt2.get()
    operator = combo.get()
    Label(window, text=f"Answer: {Calculator(number1, number2, operator).result()}").grid(column=0, row=5)


window = Tk()
window.geometry('500x300')
window.title('My firs program')
lbl = Label(window, text='Hello, my friend!', font=('Times New Roman', 15))
lbl.grid(row=0, column=3)

Label(window, text='number 1: ').grid(column=0, row=1)
txt1 = Entry(window, width=10)
txt1.grid(column=1, row=1)
Label(window, text='number 2: ').grid(column=0, row=2)
txt2 = Entry(window, width=10)
txt2.grid(column=1, row=2)
Label(window, text='operator: ').grid(column=0, row=3)
combo = Combobox(window, width=9)
combo['values'] = ('+', '-', '*', '/')
# combo.current('+')
combo.grid(column=1, row=3)

bttn = Button(window, text='Press to calculate', bg='white', fg='purple', command=clicked)
bttn.grid(column=0, row=4)

window.mainloop()
