from .exceptions_HW_13 import division_error, invalid_value


class Calculate(object):
    def __init__(self, value1, value2):
        if invalid_value(value1):
            return 'invalid value'
        else:
            self.value1 = float(value1)
        if invalid_value(value2):
            return 'Invalid value'
        else:
            self.value2 = float(value2)

    def sum(self):
        return self.value1 + self.value2

    def sub(self):
        return self.value1 - self.value2

    def mul(self):
        return self.value1 * self.value2

    def div(self):
        if division_error(self.value2):
            return "Division by zero is forbidden"
        else:
            return self.value1 / self.value2


class Calculator(Calculate):
    def __init__(self, value1, value2, operator):
        super().__init__(value1, value2)
        self.operator = operator
        self.answer = self.calculate(self.operator)

    def calculate(self, operator):
        if operator == '+':
            return self.sum()
        elif operator == '-':
            return self.sub()
        elif operator == '*':
            return self.mul()
        elif operator == '/':
            return self.div()
        else:
            return "You entered incorrect value"

    def result(self):
        return self.answer

    def __str__(self):
        return self.answer
