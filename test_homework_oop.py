from .HW_11.homework_11_2 import Car
from .HW_12.homework_12_1 import MyTime
from .HW_12.classes_HW_12_2 import Point, Figure, Circle, Triangle, Square
from .HW_13.func_HW_13 import Calculator, Calculate


# Test for task 11.2 (Testing class Car)
def test_Car():
    assert str(Car('BMW', 'X7', 2020)) == 'Brand: BMW, Model: X7, Year: 2020, Speed: 0'
    car = Car('LADA', 'VESTA', 2018, 12)
    assert str(car) == 'Brand: LADA, Model: VESTA, Year: 2018, Speed: 12'
    car.increase_the_speed()
    assert car.speed == 17
    car.decrease_the_speed()
    car.decrease_the_speed()
    assert car.speed == 7
    car.stop()
    assert car.speed == 0
    car.speed = 76
    assert car.speed == 76
    assert car.year == 2018


# Test for task 12.1 (Testing class MyTime)
def test_MyTime():
    assert str(MyTime(13, 126, 986)) == '15:22:26'
    assert str(MyTime('13,78,98')) == '14:19:38'
    assert str(MyTime()) == '00:00:00'
    assert str(MyTime(MyTime(13, 126, 986))) == '15:22:26'
    time1 = MyTime(5, 15, 75)
    time2 = MyTime(5, 135, 75)
    time3 = MyTime(5, 16, 15)
    assert not (time1 == time2)
    assert (time1 == time3)
    assert time2 != time3
    assert not (time1 > time2)
    assert time2 > time1
    assert time3 < time2
    assert time3 >= time1
    assert time3 <= time2
    assert str(time1 * 2) == '10:32:30'
    time2.set_time(12, 12, 12)
    assert time2.get_time() == '12:12:12'


# Test for task 12.2 (Testing calculate figure)
def test_calculate_figure():
    tr = Triangle(Point(2, 3), Point(2, 3), Point(7, 9))
    sq = Square(Point(5, 8), Point(10, 12))
    cl = Circle(Point(5, 8), 12)
    tr2 = Triangle(Point(2.5, 3.5), Point(5, 6), Point(4, 5))
    tr3 = Triangle(Point(0, 0), Point(0, 5), Point(0, 6))
    tr4 = Triangle(Point(0, 0), Point(0, 10), Point(10, 10))

    assert (tr.area(), tr.length()) == (-1, -1)
    assert (sq.area(), sq.length()) == (41, 26)
    assert (cl.area(), cl.length()) == (452.16, 75.36)
    assert (tr2.area(), tr2.length()) == (-1, -1)
    assert (tr3.area(), tr3.length()) == (-1, -1)
    assert (tr4.area(), tr4.length()) == (50, 34)


# Test for task 13 (Calculator)
def test_Calculator():
    assert Calculate(2, 3).sum() == 5
    assert Calculate(2, 3).sub() == -1
    assert Calculate(2, 3).mul() == 6
    assert Calculate(3, 2).div() == 1.5
    assert Calculate(134, 0).div() == "Division by zero is forbidden"

    assert Calculator(2, 3, '+').sum() == 5
    assert Calculator(2, 3, '-').result() == -1
    assert Calculator(2, 3, '*').result() == 6
    assert Calculator(3, 2, '/').div() == 1.5
    assert Calculator(134, 0, '/').result() == "Division by zero is forbidden"
