from datetime import datetime, date, timedelta
import csv


# Task 10.1
# Создать csv файл с данными следующей структуры: Имя, Фамилия,
# Возраст. Создать отчетный файл с информацией по количеству людей
# входящих в ту или иную возрастную группу. Возрастные группы: 1-12, 13-18, 19-25, 26-40, 40+

def grouping_people(fields, rows, filename, fileNameReport):
    # Create a new file
    with open(filename, 'w') as list_of_people:
        csvwriter = csv.writer(list_of_people)
        csvwriter.writerow(fields)
        csvwriter.writerows(rows)

    # Reading file, and making of report
    report = {'1-12': 0, '13-18': 0, '19-25': 0, '26-40': 0, '40+': 0}
    with open(filename) as list_of_people:
        result_list = csv.reader(list_of_people)
        for row in result_list:
            row[2].replace("\n", "")
            if row[2].isdigit():
                age = int(row[2])
                if age >= 1 and age <= 12:
                    report['1-12'] += 1
                elif age >= 13 and age <= 18:
                    report['13-18'] += 1
                elif age >= 19 and age <= 25:
                    report['19-25'] += 1
                elif age >= 26 and age <= 40:
                    report['26-40'] += 1
                elif age > 40:
                    report['40+'] += 1

    # Create a report file
    with open(fileNameReport, 'w') as report_of_group_people:
        report_file = csv.writer(report_of_group_people)
        report_file.writerow(['interval', 'count'])
        report_file.writerows([[i, j] for i, j in report.items()])


# Task 10.2
# Создать csv файл с данными о ежедневной погоде. Структура: Дата,
# Место, Градусы, Скорость ветра. Найти среднюю погоду(скорость ветра и
# градусы) для Минск за последние 7 дней

# program for create a file
def file_creator(fields, rows):
    file_name = "weather_inf.csv"
    with open(file_name, "w") as weather:
        weather_inf = csv.writer(weather)
        weather_inf.writerow(fields)
        weather_inf.writerows(rows)
    return file_name


# program for a calculate a average weather
def average_weather(file_name, today):
    # open file adn calculate resalt
    with open(file_name) as weather:
        weather_inf = csv.reader(weather)
        delta_seven = timedelta(7)
        sum_degree = 0
        sum_wind_speed = 0
        for row, value in enumerate(weather_inf):
            if row == 0:
                continue
            dt = datetime.strptime(value[0], '%Y-%m-%d').date()
            if (today - delta_seven) < dt and dt <= today:
                sum_degree += int(value[2])
                sum_wind_speed += int(value[3])

    # add calculate values in the file
    result_rows = [
        ["avg_degree", "avg_wind_speed"],
        [sum_degree / 7, sum_wind_speed / 7]
    ]
    with open(file_name, "a") as weather:
        weather_inf = csv.writer(weather)
        weather_inf.writerows(result_rows)

    # return calculate results
    return sum_degree / 7, sum_wind_speed / 7


# Task 10.3
# Дан файл, содержащий различные даты. Каждая дата - это число, месяц и
# год. Найти самую раннюю дату

def find_earliest_date(file_ame):
    with open(file_ame) as date_list:
        reader = csv.reader(date_list)
        list_of_date = []
        for i in reader:
            for j in i:
                list_of_date.append(datetime.strptime(j, '%Y-%m-%d').date())
        return min(list_of_date)
        # earliest_date = list_of_date[0]
        # for date in list_of_date:
        #     if earliest_date > date:
        #         earliest_date = date
    # return earliest_date
