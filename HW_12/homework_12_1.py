class MyTime(object):
    def __init__(self, *args):
        self.set_time(*args)

    def set_time(self, *args):
        if len(args) == 0:
            self.__hours = 0
            self.__minutes = 0
            self.__seconds = 0
        elif len(args) == 1:
            self.__converter_str_to_time(str(args[0]))
        elif len(args) == 3:
            try:
                self.__hours = int(args[0])
                self.__minutes = int(args[1])
                self.__seconds = int(args[2])
            except ValueError:
                print("Invalid string of time")
        self.__time_normalization()
        self.__time_in_seconds = self.__seconds + self.__minutes * 60 + self.__hours * 3600

    def __str__(self):
        if self.__hours < 10:
            self.__result_time = f'0{self.__hours}'
        else:
            self.__result_time = str(self.__hours)
        if self.__minutes < 10:
            self.__result_time += f':0{self.__minutes}'
        else:
            self.__result_time += f':{self.__minutes}'
        if self.__seconds < 10:
            self.__result_time += f':0{self.__seconds}'
        else:
            self.__result_time += f':{self.__seconds}'
        return self.__result_time

    def __converter_str_to_time(self, strtime):
        try:
            int(strtime[:2])
        except ValueError:
            self.__hours = 0
            print("Invalid string of hour")
        else:
            self.__hours = int(strtime[:2])

        try:
            int(strtime[3:5])
        except ValueError:
            self.__minutes = 0
            print("Invalid string of minutes")
        else:
            self.__minutes = int(strtime[3:5])

        try:
            int(strtime[6:8])
        except ValueError:
            self.__seconds = 0
            print("Invalid string of seconds")
        else:
            self.__seconds = int(strtime[6:8])

    def __time_normalization(self):
        while self.__seconds >= 60:
            self.__minutes += 1
            self.__seconds -= 60
        while self.__minutes >= 60:
            self.__hours += 1
            self.__minutes -= 60
        while self.__hours >= 24:
            self.__hours -= 24

    def __eq__(self, other):
        return self.__time_in_seconds == other.__time_in_seconds

    def __ne__(self, other):
        return self.__time_in_seconds != other.__time_in_seconds

    def __ge__(self, other):
        return self.__time_in_seconds >= other.__time_in_seconds

    def __le__(self, other):
        return self.__time_in_seconds <= other.__time_in_seconds

    def __lt__(self, other):
        return self.__time_in_seconds < other.__time_in_seconds

    def __gt__(self, other):
        return self.__time_in_seconds > other.__time_in_seconds

    def __add__(self, other):
        return MyTime(0, 0, self.__time_in_seconds + other.__time_in_seconds)

    def __sub__(self, other):
        if self.__time_in_seconds >= other.__time_in_seconds:
            return MyTime(0, 0, self.__time_in_seconds - other.__time_in_seconds)
        else:
            return "Time can not be negative"

    def __mul__(self, other):
        if other >= 0:
            return MyTime(0, 0, self.__time_in_seconds * other)
        else:
            return "Time can not be negative"

    def get_time(self):
        return str(self.__str__())
