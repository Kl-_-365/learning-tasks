import copy
import math
import random
import datetime


# Task 1.1
# Даны 2 действительных числа a и b. Получить их сумму, разность и произведение

def operations(a, b):
    return a + b, a - b, a * b


# Task 1.2
# Даны действительные числа x и y. Получить (|x|-|y|) / (1+ | xy |)

def counter(x, y):
    return (abs(x) - abs(y)) / (1 + abs(x * y))


# Task 1.3
# Дана длина ребра куба. Найти объем куба и площадь его боковой поверхности

def cube(l):
    return l ** 3, l * l * 6


# Task 1.4
# Даны два действительных числа. Найти среднее арифметическое и среднее геометрическое этих чисел

def mean(a, b):
    return (a + b) / 2


def geometric_mean(a, b):
    return (a * b) ** 0.5


# Task 1.5
# Даны катеты прямоугольного треугольника. Найти его гипотенузу и площадь

def hypotenuse(a, b):
    return (a * a + b * b) ** 0.5


def triangle_areas(a, b):
    return 0.5 * a * b


# Task 2.1
# Создать строку равную третьему символу введенной строки

def symbol_3(string):
    return string[2]


# Task 2.2
# Создать строку равную предпоследнему символу введенной строки

def sec_symbol_from_end(string):
    return string[-2]


# Task 2.3
# Создать строку равную первым пяти символам введенной строки

def first_five(string):
    return string[0:5]


# Task 2.4
# Создать строку равную введенной строку без последних двух символов

def without_two_last_symbols(string):
    return string[:-2]


# Task 2.5
# Создать строку равную всем элементам введенной строки с четными
# индексами. (считая, что индексация начинается с 0, поэтому символы
# выводятся начиная с первого, индексы 0,2,4,6….)

def even_symbols(string):
    return string[::2]


# Task 3.1
# Введите число. Если это число делиться на 1000 без остатка, то выведите на экран "millennium"

def millennium(number):
    if number % 1000 == 0:
        return 'millennium'
    return False


# Task 3.2
# В семье N свадьба. Они решили выбрать заведение, где будут праздновать в
# зависимости от количества людей, которое прибудет к утру.
# Если их будет больше 50 - закажут ресторан, если от 20 до 50 -то кафе, а если
# меньше 20 то отпраздную дома.
# Вывести "ресторан", "кафе", "дом" в зависимости от количества гостей (прочитать
# переменную с консоли)

def wedding(people_amount):
    if people_amount > 50:
        return "Ресторан"
    elif people_amount > 20:
        return "Кафе"
    elif people_amount > 0:
        return "Дом"
    elif people_amount == 0:
        return "Свадьба отменяется"
    return False


# Task 3.3
# Ввести строку. Если длина строки больше 10 символов, то создать новую
# строку с 3 восклицательными знаками в конце ('!!!') и вывести на экран.
# Если меньше 10, то вывести на экран второй символ строки

def str_len(string):
    if len(string) > 10:
        return string + "!!!"
    else:
        return string[1]


# Task 3.4
# Ввести строку. Вывести на экран букву, которая находится в середине этой строки.
# Также, если эта центральная буква равна первой букве в строке, то создать и
# вывести часть строки между первым и последним символами исходной строки.
# (подсказка: для получения центральной буквы, найдите длину строки и разделите
# ее пополам. Для создания результирующий строки используйте срез)

def print_center_letter(string):
    string = str(string)
    if string[math.ceil(len(string) / 2) - 1] == string[0]:
        return string[math.ceil(len(string) / 2) - 1], string[1:-1]
    else:
        return string[math.ceil(len(string) / 2) - 1]


# Task 4.1
# Дан список целых чисел.
# Создать новый список, каждый элемент которого равен исходному элементу умноженному на -2
def mult_neg_2(some_list):
    new_list = []
    for i in some_list:
        new_list.append(i * (-2))
    return new_list


# Task 4.2
# Дан список целых чисел. Подсчитать сколько четных чисел в списке
def even_list(some_list):
    even_numbers = []
    for i in some_list:
        if i % 2 == 0:
            even_numbers.append(i)
    return even_numbers


# Task 4.3
# Дан словарь: {'test': 'test_value', 'europe': 'eur', 'dollar': 'usd', 'ruble': 'rub'}
# Добавить каждому ключу число равное длине этого ключа (пример {‘key’: ‘value’} -> {‘key3’:
# ‘value’}). Чтобы получить список ключей - использовать метод .keys()
def dict_edit(dictionary):
    new_dict = {}
    for i in dictionary.keys():
        new_dict[i + str(len(i))] = dictionary.get(i)
    return new_dict


# Task 4.4
# Дан список. Создать новый список, сдвинутый на 1 элемент влево
# Пример: 1 2 3 4 5 -> 2 3 4 5 1
def shift_list(some_list):
    new_list = some_list[1:]
    new_list.append(some_list[0])
    return new_list


# Task 4.5
# Составить список чисел Фибоначчи содержащий 15 элементов
def fibonacci_list():
    list_of_fibonacci = [1, 1]
    for i in range(1, 14):
        list_of_fibonacci.append(list_of_fibonacci[i] + list_of_fibonacci[i - 1])
    # ниже решение циклом while
    # i = 1
    # list_of_fibonacci = [1, 1]
    # while i < 14:
    #     list_of_fibonacci.append(list_of_fibonacci[i] + list_of_fibonacci[i - 1])
    #     i+=1
    return list_of_fibonacci


# Task 5.1
# Написать программу, в которой вводятся два операнда Х и Y и знак операции
# sign (+, –, /, *). Вычислить результат Z в зависимости от знака. Предусмотреть
# реакции на возможный неверный знак операции, а также на ввод Y=0 при
# делении. Организовать возможность многократных вычислений без перезагрузки
# программа (т.е. построить бесконечный цикл). В качестве символа прекращения
# вычислений принять ‘0’ (т.е. sign='0')

def simple_calc(x, y, operator):
    x = float(x)
    y = float(y)
    if operator == "+":
        return x + y
    elif operator == "-":
        return x - y
    elif operator == "*":
        return x * y
    elif operator == "/":
        if y == 0:
            return "ZeroDivisionError"
        return x / y
    else:
        return "ErrorOperator"


# Task 5.2
# Дано число. Найти сумму и произведение его цифр

def sum_elements_of_numbers(nmb):
    nmb = str(nmb)
    summ = 0
    multiply = 1
    for i in nmb:
        summ += int(i)
        multiply *= int(i)
    return summ, multiply


# Task 5.3
# Два натуральных числа называют дружественными, если каждое из них
# равно сумме всех делителей другого, кроме самого этого числа. Найти все
# пары дружественных чисел, лежащих в диапазоне от 200 до 300

def friendly_checker():
    list_friendly_numbers = []
    for i in range(200, 301):
        sum_divider = sum(j for j in range(1, i) if i % j == 0)
        if i == sum(j for j in range(1, sum_divider) if sum_divider % j == 0):
            list_friendly_numbers.append((i, sum_divider))
    return list_friendly_numbers


# Task 5.4
# Для заданного числа N составьте программу вычисления суммы
# S=1+1/2+1/3+1/4+...+1/N, где N – натуральное число

def get_sum(num):
    summ = 0
    num = int(num)
    for i in range(1, num + 1):
        summ += 1 / i
    return summ


# Task 5.5
# В массиве целых чисел с количеством элементов 19 определить максимальное
# число и заменить им все четные по значению элементы

def changed_by_max_value(array):
    for index, value in enumerate(array):
        if value % 2 == 0:
            array[index] = max(array)
    return array


# Task 5.6
# Задан целочисленный массив. Определить количество участков массива,
# на котором элементы монотонно возрастают (каждое следующее число
# больше предыдущего)

def incr(sp):
    counter = 0
    increase_in_past = False
    for i in range(len(sp[:-1])):
        if sp[i + 1] > sp[i] and not increase_in_past:
            counter += 1
        increase_in_past = sp[i + 1] > sp[i]
    return counter


# Task 5.7
# Дана целочисленная квадратная матрица. Найти в каждой строке наибольший
# элемент и поменять его местами с элементом главной диагонали

def change_number(matrix):
    for i in range(len(matrix)):
        index_max_value = matrix[i].index(max(matrix[i]))
        swap = matrix[i][i]
        matrix[i][i] = matrix[i][index_max_value]
        matrix[i][index_max_value] = swap
    return matrix


# Task 5.8
# В заданной строке расположить в обратном порядке все слова. Разделителями
# слов считаются пробелы

def str_reverse(stroka):
    list = stroka.split(" ")
    list.reverse()
    new_stroka = ' '.join(list)
    return new_stroka


# Task 5.9
# Для каждого натурального числа в промежутке от m до n вывести все делители,
# кроме единицы и самого числа. m и n вводятся с клавиатуры.
# Пример:m =100, n = 105
# 100: 2 4 5 10 20 25 50
# 101:
# 102: 2 3 6 17 34 51
# 103:
# 104: 2 4 8 13 26 52
# 105: 3 5 7 15 21 35

def dividers(m, n):
    dict_of_dilivers = {}
    for i in range(m, n + 1):
        dict_of_dilivers[i] = ([j for j in range(2, i) if i % j == 0])
    return dict_of_dilivers


# Task 5.10
# Создать список поездов. Структура словаря: номер поезда,
# пункт и время прибытия, пункт и время отбытия. Вывести все сведения о поездах,
# время пребывания в пути которых превышает 7 часов 20 минут

def info_trains(trains):
    dict_of_longway_trains = {}
    for i in trains:
        time_arrival = trains.get(i)[0][1]
        time_departure = trains.get(i)[1][1]
        if time_arrival < time_departure:
            return 'Incorrect data!'
        if time_departure + datetime.timedelta(hours=7, minutes=20) < time_arrival:
            dict_of_longway_trains[i] = trains.get(i)
    return dict_of_longway_trains


# Task 6.1
# Создать матрицу случайных чисел от a до b, размерность матрицы n*m

def create_matrix(start_number, end_number, num_rows, num_columns):
    # with the list generator
    return [[random.randint(start_number, end_number) for j in range(num_columns)] for i in range(num_rows)]

    ## with the cycles
    # array = []
    # for i in range(num_rows):
    #     list_inside_matrix = []
    #     for j in range(num_columns):
    #         list_inside_matrix.append(random.randint(start_number, end_number))
    #     array.append(list_inside_matrix)
    # return array


# Task 6.2
# Найти максимальный элемент матрицы

def get_max_number(array):
    list_max_numbers = []
    for i in array:
        list_max_numbers.append(max(i))
    return max(list_max_numbers)


# Task 6.3
# Найти минимальный минимальный матрицы

def get_min_number(array):
    list_min_numbers = []
    for i in array:
        list_min_numbers.append(min(i))
    return min(list_min_numbers)


# Task 6.4
# Найти сумму всех элементов матрицы

def get_sum_all_numbers(array):
    summ = 0
    for i in array:
        for j in i:
            summ += j
    return summ


# Task 6.5
# Найти индекс ряда с максимальной суммой элементов

def get_index_row_max_sum_numbers(array):
    list_max_numbers = []
    for i in array:
        list_max_numbers.append(sum(i))
    return list_max_numbers.index(max(list_max_numbers))


# Task 6.6
# Найти индекс колонки с максимальной суммой элементо

def get_index_column_max_sum_numbers(array):
    list_max_numbers = []
    for i in range(len(array[0])):
        summ = 0
        for j in range(len(array)):
            summ += array[j][i]
        list_max_numbers.append(summ)
    return list_max_numbers.index(max(list_max_numbers))


# Task 6.7
# Найти индекс ряда с минимальной суммой элементов

def get_index_row_min_sum_numbers(array):
    list_max_numbers = []
    for i in array:
        list_max_numbers.append(sum(i))
    return list_max_numbers.index(min(list_max_numbers))


# Task 6.8
# Найти индекс колонки с минимальной суммой элементов

def get_index_column_min_sum_numbers(array):
    list_max_numbers = []
    for i in range(len(array[0])):
        summ = 0
        for j in range(len(array)):
            summ += array[j][i]
        list_max_numbers.append(summ)

    return list_max_numbers.index(min(list_max_numbers))


# Task 6.9
# Обнулить все элементы выше главной диагонали

def set_zero_elements_above_main_diagonal(array):
    for i in range(len(array)):
        for j in range(i + 1, len(array[i])):
            array[i][j] = 0
    return array


# Task 6.10
# Обнулить все элементы ниже главной диагонали

def set_zero_elements_below_main_diagonal(array):
    for i in range(1, len(array)):
        for j in range(0, i):
            array[i][j] = 0
    return array


# Task 6.11
# Создать две новые матрицы matrix_a, matrix_b случайных чисел размерностью n*m

def create_two_matrix(m, n):
    matrix_a = create_matrix(-100000, 100000, m, n)
    matrix_b = create_matrix(-100000, 100000, m, n)

    return matrix_a, matrix_b


# Task 6.12
# Создать матрицу равную сумме matrix_a и matrix_b

def get_summ_matrix(matrix_a, matrix_b):
    new_matrix = []
    for i in range(len(matrix_a)):
        new_list_for_matrix = []
        for j in range(len(matrix_a[i])):
            new_list_for_matrix.append(matrix_a[i][j] + matrix_b[i][j])
        new_matrix.append(new_list_for_matrix)

    return new_matrix


# Task 6.13
# Создать матрицу равную разности matrix_a и matrix_b

def get_sub_matrix(matrix_a, matrix_b):
    new_matrix = []
    for i in range(len(matrix_a)):
        new_list_for_matrix = []
        for j in range(len(matrix_a[i])):
            new_list_for_matrix.append(matrix_a[i][j] - matrix_b[i][j])
        new_matrix.append(new_list_for_matrix)
    return new_matrix


# Task 6.14
# Создать новую матрицу равную matrix_a умноженной на g. g вводится с клавиатуры

def get_multipl_g_matrix(g, matrix):
    new_matrix = []
    for i in range(len(matrix)):
        new_matrix_list = []
        for j in range(len(matrix[i])):
            new_matrix_list.append(matrix[i][j] * g)
        new_matrix.append(new_matrix_list)
    return new_matrix


# Task 7
# Написать 12 функций по переводу:
# Дюймы в сантиметры
# Сантиметры в дюймы
# Мили в километры
# Километры в мили
# Фунты в килограммы
# Килограммы в фунты
# Унции в граммы
# Граммы в унции
# Галлон в литры
# Литры в галлоны
# Пинты в литры
# Литры в пинты

def inch_to_cm(inch):
    return inch * 2.54


def cm_to_inch(cm):
    return cm / 2.54


def ml_to_km(mm):
    return mm * 1.609


def km_to_ml(km):
    return km / 1.609


def ft_to_kg(ft):
    return ft / 2.205


def kg_to_ft(kg):
    return kg * 2.205


def oz_to_g(oz):
    return oz * 28.3495


def g_to_oz(g):
    return g / 28.3495


def gallon_to_l(gallon):
    return gallon * 3.785


def l_to_gallon(l):
    return l / 3.785


def pint_to_l(pint):
    return pint / 2.113


def l_to_pint(l):
    return l * 2.113


# Task 8.1
# Описать функцию fact2( n ), вычисляющую двойной факториал :n!! =
# 1·3·5·...·n, если n — нечетное; n!! = 2·4·6·...·n, если n — четное (n > 0 —
# параметр целого типа. С помощью этой функции найти двойные факториалы пяти данных целых чисел

def double_factorial(x):
    # double_factorial = 1
    # for i in range(x, 1, -2):
    #     double_factorial *= i
    # return double_factorial
    if x <= 0:
        return 1
    return x * double_factorial(x - 2)


# Task 8.2
# Даны три слова. Выяснить, является ли хоть одно из них палиндромом
# ("перевертышем"), т. е. таким, которое читается одинаково слева направо и
# справа налево. (Определить функцию, позволяющую распознавать слова палиндромы.)

def polindrom(word):
    return word == word[::-1]


def polindrom_in_polindroms(list_of_words):
    for i in list_of_words:
        if polindrom(i):
            return "There is a polydrome"
    return "There isn't a polydrome"


# Task 8.3
# Описать функцию Sin1( x , ε ) вещественного типа (параметры x , ε —
# вещественные, ε > 0), находящую приближенное значение функции sin( x ):
# sin( x ) = x – x ^3 /(3!) + x^ 5 /(5!) – ... + (–1) ^ n · x^( 2·n+1) /((2· n +1)!) + ... .
# В сумме учитывать все слагаемые, модуль которых больше ε . С помощью
# Sin1 найти приближенное значение синуса для данного x при шести данных ε

def sin1(x, eps):
    while (x > 2 * math.pi):
        x -= 2 * math.pi
    if eps <= 0:
        return "Use the epselon more thad 0"
    result_expression = x
    intermediate_result = x
    n = 1
    while abs(intermediate_result) > eps:
        intermediate_result = (((-1) ** n) * (x ** (2 * n + 1))) / (factorial(2 * n + 1))
        n += 1
        result_expression += intermediate_result
    return result_expression


def factorial(n):
    if n == 0:
        return 1
    return n * factorial(n - 1)


def list_sin_with_different_precision(x, list_eps):
    return [sin1(x, eps) for eps in list_eps]


# Task 9.1
# Дан список строк. Отформатировать все строки в формате ‘{i} - {string}’, где i
# это порядковый номер строки в списке. Использовать генератор списков

def format(list):
    return [f'{i + 1} - {v}' for i, v in enumerate(list)]


# Task 9.2
# Создать lambda функцию, которая принимает на вход неопределенное
# количество именных аргументов и выводит словарь с ключами удвоенной
# длины. {‘abc’: 5} -> {‘abcabc’: 5}

def doubling_arguments(**kwargs):
    return dict(map(lambda x: [x * 2, kwargs[x]], kwargs.keys()))


# Task 9.3
# Создать декоратор для функции, которая принимает список чисел.
# Декоратор должен производить предварительную проверку данных -
# удалять все четные элементы из списка

def decorator(fun):
    def wrapper(list_of_numbers):
        return fun([i for i in list_of_numbers if i % 2 != 0])

    return wrapper


@decorator
def numbers(list_numbers):
    return list_numbers


# Task 9.4
# Создать универсальный декоратор, который меняет порядок аргументов в
# функции на противоположный

def decorator_reverse_args(fun):
    def wrapper(*args):
        if len(args) == 1:
            return args[0]
        new_args = args[::-1]
        return fun(*new_args)

    return wrapper


@decorator_reverse_args
def decorating_function(*args):
    return args
